const { app, BrowserWindow } = require('electron')
const { ipcMain } = require('electron')
const { spawn } = require('child_process')

let win = null

function createWindow () {
  let win = new BrowserWindow({ width: 800, height: 600, frame: false, minWidth: 800, minHeight: 600 })

  win.loadFile('src/index.html')

  win.on('closed', () => {
    win = null;
  })

  //      Window Controls
  ipcMain.on('close-window', (event, arg) => {
    win.close()
  })

  ipcMain.on('max-window', (event, arg) => {
    if (win.isMaximized()) {
      win.unmaximize()
    } else {
      win.maximize()
    }
  })

  ipcMain.on('min-window', (event, arg) => {
    win.minimize()
  })

  ipcMain.on('nordexec', (event, arg) => {
    var command = spawn('nordvpn', arg.split(' '))
    command.stdout.on('data', (data) => {
      console.log(data.toString('utf8'))
      if (data.toString('utf8').includes('Connecting to')) {
        event.sender.send('connecting', data.toString('utf8').match('[a-z]*[0-9]*.nordvpn.com'))
      } else if (data.toString('utf8').includes('Great! You')) {
        event.sender.send('connected', data.toString('utf8').match('[a-z]*[0-9]*.nordvpn.com'))
      } else if (data.toString('utf8').includes('disconnected')) {
        event.sender.send('disconnected', 'Pick country or use quick connect.')
      } else if (data.toString('utf8').includes('an issue. Please')) {
        event.sender.send('disconnected', 'There is a problem with your internet connection. Try again.')
      } else if (data.toString('utf8').includes('You are already connected')) {
        event.sender.send('get-status', 'connected')
      } else if (data.toString('utf8').includes('Current server:')) {
        event.sender.send('connected', data.toString('utf8').match('[a-z]*[0-9]*.nordvpn.com'))
      } else if (data.toString('utf8').includes('is successfully set to')) {
        event.sender.send('trigger-settings-refresh', '')
      } else if (data.toString('utf8').includes('DNS is')) {
        event.sender.send('trigger-settings-refresh', '')
      } else if (data.toString('utf8').includes('Auto connect is successfully')) {
        event.sender.send('trigger-settings-refresh', '')
      }
    })
  })

  ipcMain.on('get-settings', (event, arg) => {
    var settings = spawn('nordvpn', ['settings'])
    settings.stdout.on('data', (data) => {
      var buffer = data.toString()
      var dns = ''
      if (buffer.match(/DNS: [0-9]*.[0-9]*.[0-9]*.[0-9]*, [0-9]*.[0-9]*.[0-9]*.[0-9]*/) == null) {
        dns = 'DNS: disabled'
      } else { dns = buffer.match(/DNS: [0-9]*.[0-9]*.[0-9]*.[0-9]*, [0-9]*.[0-9]*.[0-9]*.[0-9]*/) }

      var list = [
        buffer.match(/Protocol: [A-Z]*/),
        buffer.match(/Kill Switch: [a-z]*/),
        buffer.match(/CyberSec: [a-z]*/),
        buffer.match(/Obfuscate: [a-z]*/),
        buffer.match(/Auto connect: [a-z]*/),
        dns
      ]
      event.sender.send('settings', list)
    })
  })

  ipcMain.on('debug', (event, arg) => {
    console.log('arg')
  })

  win.webContents.on('did-finish-load', () => {
    var startup = spawn('nordvpn', ['status'])
    startup.stdout.on('data', (data) => {
      if (data.toString('utf8').includes('Current server:')) {
        win.webContents.send('connected', data.toString('utf8').match('[a-z]*[0-9]*.nordvpn.com'))
      } else {
        win.webContents.send('disconnected', 'Pick country or use quick connect.')
      }
    })
    var version = spawn('nordvpn', ['-v'])
    version.stdout.on('data', (data) => {
      var buffer = data.toString('utf8')
      if (buffer.includes('Version')) {
        win.webContents.send('version', buffer.match(/[0-9]{1,}.[0-9]{1,}.[0-9]{1,}/))
      }
    })
    win.webContents.send('trigger-settings-refresh', '')
  })
}

app.on('ready', createWindow)