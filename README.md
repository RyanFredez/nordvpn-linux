NordVPN Linux
=====
> A Linux GUI frontend for NordVPN CLI written in Electron

NordVPN has yet to release a Linux GUI frontend for their CLI. This application allows you to interface with NordVPN CLI in a user friendly way.

Install
-----

##### Ubuntu
First install the CLI application

```bash
wget https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb
sudo apt-get install nordvpn-release_1.0.0_all.deb
sudo apt-get update
sudo apt-get install nordvpn
```

*The snap package is currently failing to run because the confinement was set to strict. The snap package is unavailable until further notice.*